# README #

### showSDR Script ###

* This script shows lonely sdr-folders and new files which have not been opened on the Kindle.
* Version: 1

### How do I get set up? ###

* Download **showSDR.sh**.
* Make it executable.
```
#!Shell
chmod +x showSDR.sh
```
* Use it.

```
#!Shell
# Without path
./showSDR.sh
```

```
#!Shell
# With path
./showSDR.sh BooksFromUSB
./showSDR.sh BooksFromUSB/
./showSDR.sh ./BooksFromUSB/
```


### Who do I talk to? ###

* Repo owner