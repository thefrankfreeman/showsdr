#!/bin/sh
##############################################################################
#  Name:
#  showSDR.sh
#
#  Description:
#  This script show lonely sdr-folders and new files which have not been
#  opened on the Kindle.
##############################################################################

# Set path according to given commandline argument
if [[ -z $1 ]]; then
    workPath=
else
    workPath=$(echo $1 | sed -e 's/\/$//' -e 's/$/\//')
fi

# Remove the extensions
ls -d ${workPath}*.sdr | sed -r 's/\.sdr//' >sdr.tmp

ls -d ${workPath}* | egrep '\.((m|p)obi|txt|pdf|azw(3)?)$' | \
 sed -r 's/\.((m|p)obi|txt|pdf|azw(3)?)$//' >book.tmp

# Print files to delete and new files in different color
colordiff sdr.tmp book.tmp

# Clean up
rm sdr.tmp book.tmp

